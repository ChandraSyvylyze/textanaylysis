'''NOTE: DO NOT USE THIS FILE. IT  IS FOR DEVELOPMENT PURPOSE ONLY'''


import re
import pickle
import pandas as pd

# Read the csv file into panda dataframe
df = pd.read_csv('/media/dell/HDD_VOL1/syvylyze_Analytics/textanalysis/textanaylysis/WordCloudSampleData.csv')

# get the content of column named 'content'
contentDf = df['content']

stopWords = {'asking', 'thank', 'hence', 'somewhere', 'entirely', 'f', 'thus', 'thanx', 'just', 'taking', 'were',
             'inside',
             'where', 'caption', 'front', 'by', 'mostly', 'whatever', 'mrs', 'she', 'off', 'among', 'change',
             'indicate',
             'currently', 'has', 'buys', 'gotten', 'hardly', 'becoming', 'this', 'fifty', 'round', 'less', 'follows',
             'amoungst', 'my', 'though', 'cause', 'very', 'twenty', 'had', 'during', 'whenever', 'i', 'about', 'h',
             'meantime',
             'nevertheless', 'ours', 'r', 'are', 'fifteen', 'bit', 'two', 'due', 'lest', 'yourself', 'since',
             'anywhere',
             'went', 'doesn', 'ive', 'thick', 'saw', 'thing', 'sincere', 'see', 'fill', 'done', 'as', 'minus',
             'elsewhere',
             'know', 'therefore', 'after', 'try', 'themselves', 'call', 'itself', 'wishlist', 'ignored', 'these', 'ml',
             'hereafter', 'eight', 'yes', 'exactly', 'therein', 'will', 'b', 'took', 'at', 'till', 'various', 'herein',
             'must',
             'only', 'get', 'happens', 'five', 'no', 'thereupon', 'yet', 'over', 'nothing', 'onto', 'gets', 'beside',
             'dare',
             'j', 'me', 'de', 'together', 've', 'let', 'more', 'non', 'pro', 'namely', 'yourselves', 'anything', 'l',
             'it',
             'either', 't', 'ourselves', 'upon', 'her', 'farther', 'hasnt', 'should', 'we', 'hes', 'e', 'become',
             'nine',
             'awfully', 'never', 'hopefully', 'versus', 'in', 'few', 'whichever', 'therere', 'willing', 'above', 'miss',
             'itll',
             'shall', 'u', 'w', 'almost', 'formerly', 'everybody', 'what', 'couldnt', 'describe', 'used', 'somewhat',
             'keep',
             'once', 'cery', 'was', 'whole', 'whereby', 'cannot', 'them', 'rather', 'system', 'following', 'be', 'of',
             'nd',
             'really', 'towards', 'hv', 'without', 'ex', 'm', 'cry', 'c', 'ask', 'eg', 'to', 'seeming', 'away', 'able',
             'every',
             'put', 'out', 'mainly', 'tell', 'tha', 'got', 'detail', 'alone', 'amount', 'latterly', 'still', 'alot',
             'oh',
             'each', 'bottom', 'who', 'forever', 'sub', 'besides', 'otherwise', 'getting', 'up', 'gone', 'long',
             'taken',
             'lower', 'per', 'name', 'known', 'full', 'plus', 'somehow', 'unless', 'not', 'backward', 'rs', 'gud', 'k',
             'made',
             'behind', 'anyhow', 'followed', 'keeps', 'ie', 'mustn', 'wasn', 'back', 'ever', 'make', 'does', 'd',
             'else', 'go',
             'provide', 'thence', 'shed', 'someone', 'within', 'best', 'un', 'through', 'lasts', 'thereafter', 'thru',
             'from',
             'arent', 'con', 'might', 'between', 'kg', 'why', 'comes', 'own', 'begin', 'toward', 'below', 'all', 'whom',
             'neither', 'again', 'ought', 'll', 'inc', 'something', 'down', 'some', 'several', 'beforehand', 'whose',
             'came',
             'well', 'doesnt', 're', 'especially', 'except', 'herself', 'helped', 'but', 'indeed', 'goes', 'find',
             'nobody',
             'q', 'first', 'have', 'hasn', 'for', 'better', 'y', 'your', 'interest', 'selves', 'may', 'theyd', 'please',
             'upwards', 'further', 'inner', 'fewer', 'needs', 'unlike', 'such', 'latter', 'third', 'self', 'hereby',
             'iam',
             'knows', 'move', 'mr', 'one', 'p', 'near', 'ending', 'going', 'take', 'often', 'sup', 'az', 'already',
             'against',
             'than', 'both', 'wherein', 'am', 'far', 'can', 'ane', 'meanwhile', 'three', 'other', 'if', 'others',
             'half',
             'under', 'hers', 'having', 'maybe', 'thereby', 'z', 'do', 'mean', 'side', 'noone', 'via', 'certainly',
             'into', 'a',
             'ill', 'hello', 'theyll', 'contain', 'with', 'across', 'you', 'won t', 'undoing', 'viz', 'throughout',
             'shell',
             'look', 'makes', 'nor', 'before', 'beyond', 'brief', 'much', 'always', 'nowhere', 'kept', 'certain',
             'that',
             'another', 'top', 'cal', 'wherever', 'g', 'v', 'whence', 'did', 'our', 'didnt', 'likewise', 'thats',
             'upward',
             'same', 'so', 'mm', 's', 'whither', 'most', 'seems', 'their', 'although', 'wasnt', 'its', 'definitely',
             'mine',
             'also', 'cant', 'sure', 'him', 'im', 'because', 'anyone', 'o', 'even', 'empty', 'using', 'gives',
             'whereupon',
             'mill', 'fairly', 'becomes', 'six', 'example', 'moreover', 'sixty', 'wid', 'doing', 'everyone', 'they',
             'described', 'found', 'mg', 'clearly', 'he', 'us', 'his', 'unto', 'afterwards', 'amidst', 'sometime',
             'whether',
             'hi', 'many', 'don', 'perhaps', 'giving', 'yours', 'forty', 'around', 'changes', 'four', 'everywhere',
             'show',
             'hadnt', 'sometimes', 'therell', 'whereas', 'given', 'amongst', 'became', 'amid', 'normally', 'himself',
             'along',
             'ten', 'none', 'seemed', 'those', 'how', 'km', 'fire', 'old', 'plz', 'n', 'twelve', 'hrs', 'hadn',
             'mustnt',
             'tends', 'overflow', 'or', 'provides', 'etc', 'use', 'bill', 'least', 'youve', 'then', 'however', 'seem',
             'come',
             'appear', 'alongside', 'last', 'ltd', 'inward', 'now', 'need', 'wish', 'myself', 'befire', 'didn', 'would',
             'could', 'buz', 'whoever', 'on', 'overall', 'eleven', 'former', 'part', 'serious', 'while', 'and', 'say',
             'here',
             'everything', 'give', 'ok', 'any', 'x', 'apart', 'hereupon', 'onone', 'anyway', 'co', 'pls', 'next', 'is',
             'an',
             'hundred', 'regarding', 'which', 'been', 'new', 'indicates', 'until', 'it s', 'consider', 'there', 'the',
             'enough',
             'looking', 'when', 'too', 'things', 'being', 'whereafter', 'low'}

# print(len(stopWords))
wordFrequencyDict = dict()

for text in contentDf:
    # Raw data
    text = text.lower()
    # print(text)
    # remove all special character (non alphabets)

    filterText = re.sub('[^A-Za-z]+', ' ', text)
    # print(filterText)
    filterTextWords = filterText.split()
    strWithoutStopWords = [word for word in filterTextWords if word not in stopWords]
    result = ' '.join(strWithoutStopWords)
    # print(result)  # remove the stopwords
    # print("*******")

    # count the word frequency
    for item in strWithoutStopWords:
        if item in wordFrequencyDict:
            wordFrequencyDict[item] = wordFrequencyDict[item] + 1
        else:
            wordFrequencyDict[item] = 1

sorted_word_freq_dict = dict(sorted(wordFrequencyDict.items(), key=lambda element: element[1], reverse=True))

for pair in sorted_word_freq_dict.items():
    print(pair)

print(len(sorted_word_freq_dict))

file_name = 'word_colud.pkl'
f = open(file_name, 'wb')
pickle.dump(sorted_word_freq_dict, f)
f.close()
