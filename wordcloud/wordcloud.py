import re
import pandas as pd
import sys
import pickle
from constants import stopWords, BAD_FILE_OR_BAD_FILE_PATH
from constants import CORRECT_USAGE

WORD_CLOUD_PICKLE_FILE = 'word_cloud.pkl'
WORD_CLOUD_FILTERED_CSV = 'wordCloudFilterData.csv'


word_frequency_dict = dict()


def parse_file_and_get_dataframe(path):
    try:
        df = pd.read_csv(path)
    except FileNotFoundError:
        print(BAD_FILE_OR_BAD_FILE_PATH)
        sys.exit(-1)
    except PermissionError:
        print("Permission denied")
        sys.exit(-1)
    return df['content']


def write_to_pickle_file(sorted_word_freq_dict):
    file_name = WORD_CLOUD_PICKLE_FILE
    f = open(file_name, 'wb')
    pickle.dump(sorted_word_freq_dict, f)
    f.close()


# This function is used only for verification purpose. It has no role in actual coding.
def generate_filtered_column(filter_content, content_df):
    content_df["filterContent"] = filter_content
    content_df.to_csv(WORD_CLOUD_FILTERED_CSV, index=False)
    print('File created successfully')


def remove_stopwords_and_nonalphabets(content_series, filter_content):
    for text in content_series:
        text = text.lower()
        # print(text)
        filter_text = re.sub('[^A-Za-z]+', ' ', text)
        filter_text_words = filter_text.split()
        str_without_stopwords = [word for word in filter_text_words if word not in stopWords]
        filter_content.append(' '.join(str_without_stopwords))

        for item in str_without_stopwords:
            if item in word_frequency_dict:
                word_frequency_dict[item] = word_frequency_dict[item] + 1
            else:
                word_frequency_dict[item] = 1
    sorted_word_freq_dict = dict(sorted(word_frequency_dict.items(), key=lambda element: element[1], reverse=True))
    # Debug statement
    for pair in sorted_word_freq_dict.items():
        print(pair)
    print(len(sorted_word_freq_dict))  # Debug statement
    write_to_pickle_file(sorted_word_freq_dict)
    return filter_content


def get_word_frequency(path):
    filterContent = list()
    content_series = parse_file_and_get_dataframe(path)
    content_df = pd.DataFrame(content_series)
    filter_content = remove_stopwords_and_nonalphabets(content_series, filterContent)
    generate_filtered_column(filter_content, content_df)


if __name__ == '__main__':
    if len(sys.argv) != 2:
        raise ValueError(CORRECT_USAGE)

    else:
        get_word_frequency(sys.argv[1])
